// Muhammad Reza Bintami
const express = require('express');
const bParser = require('body-parser');
const my = require('mysql2');
const { json } = require('express');
const db = my.createConnection({
    host: 'localhost',
    user: 'reza',
    password: '123',
    database: 'go_coba'
})
const app = express();
app.use(bParser.urlencoded({
    extended: false
}));

app.get('/getProduct', function (req, res) {
    let jsonku = {}
    let q = `Select sku,product_name,stocks from products ORDER BY sku DESC`
    db.query(q, function (err, rows) {
        if (err) throw err
        jsonku.data = rows
        jsonku.deployer = "Muhammad Reza Bintami"
        return res.send(JSON.stringify(jsonku))
    })
})


app.listen(5432, function () {
    console.log('listened at port 5432')
})